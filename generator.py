#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 26 16:21:13 2019

@author: raissa
"""

import random
import pickle 
from main import *
import subprocess

"n = taille de la grille"
n = 3

""" =========== GENERATION DE GRILLE SIMPLE A ETRE SOLVED =========== """

def position_aleatoire(n):
    """ Pour une grille de taille n génère une liste de taille n**2 de nombre aléatoires entre 0 et n**4.
    Ces nombres correspondent à l'emplacement d'un nombre k (entre 1 et n**2) sur la string qui génère la grille
    (cf. gen_grille). """
    i = 0
    cond_init_alea = []
    for k in range(0,n**2):
        i = random.randint(0,n**4)
        cond_init_alea.append(i)
    return cond_init_alea

def gen_grille(n):
    """ Étant donnée une liste d'emplacements des nombres k, crée une chaîne de caractères où:
        * = case vide
        k = nombre dans cette case
        \n = retour à la ligne pour former la grille. """
    cond_init_alea = position_aleatoire(n)
    s = str(n) + '\n'
    for i in range(0,n**4):
        if i != 0 and (i%(n**2)) == 0:
            s += '\n'
        if i in cond_init_alea:
            num = str(cond_init_alea.index(i)+1) 
            s += num
        else:
            s += '*'
    s += '\n'
    return s
        
def export_grille_txt(filename,n):
    """ Exporte la grille en fichier texte lisible par le solveur de sudoku. La taille n de la grille
    est en début de fichier, avant la grille elle-même. """
    s_grille = gen_grille(n)
    file = open(SUDOKUDIR+filename + ".txt" , "w") 
    file.write(s_grille)
    file.close()

""" ========== SOLUTION GRILLE + SUPPRESSION ALEA DE CASES ============= """  
  
def mat_to_list(mat,n):
    """ Transforme sudoku résolu écrit sous forme matricielle en une liste."""
    new_list = []
    for i in range(0,n**2):
        for j in range(0,n**2):
            new_list.append(mat[i][j])
    return new_list

def solve_mat(filename,n):
    """ Solve une grille de sudoku (a priori supposée satisfiable sinon retourne rien).
    Si possible la retourne sous forme d'une matrice, sinon ne retourne rien (= pb potentiel) """
    with open(SUDOKUDIR+filename+"pickle.pck","rb") as file:
        sudoku,n = pickle.load(file)
    with open(SUDOKUDIR+filename+"result.txt") as file:
        result = file.read()
    if result[:3] != "SAT":
        print("Insatisfaisable")
        return
    result = result[4:]
    result = split_result(result)
    for v in result:
        if v < 0:
            continue
        i,j,k = n_to_ijk(v,n**2)
        sudoku[i][j] = k+1
    return sudoku

def suppression(liste_solved,n,m):
    """ Tire au sort des positions (sous forme d'indices d'une liste). Remplace les nombres attribués à cette
    position par le SATsolver par une étoile. Retourne une nouvelle liste."""
    memo = []
    while len(memo)<(n**4-m):
        i = random.randint(0,n**4-1)
        if i not in memo:
            memo.append(i)
    for j in memo:
        liste_solved[j] = '*'
    return liste_solved

def format_to_string(liste_solved,n):
    """ Formate une liste en format adapté pour visualisation grille par le fichier texte (avec l'info n en 
    début pour le SATsolver)."""
    s = str(n)+'\n'
    for i in range(0,n**4):
        if i != 0 and (i%(n**2)) == 0:
            s += '\n'
        s += str(liste_solved[i])
    s += '\n'
    return s

def export_result_txt(filename,s_export):
    """ Exporte la grille en fichier texte lisible par le solveur de sudoku. La taille n de la grille
    est en début de fichier, avant la grille elle-même. """
    file = open(SUDOKUDIR+filename + ".txt" , "w") 
    file.write(s_export)
    file.close()


""" ========= FONCTION FINALE ========== """

def gen_grille_finale(filename,n,m):
    """ Créer un fichier texte contenant une grille incomplète, satisfiable.
    Variables:
    filename = nom qui auront les fichiers qu'on va créer 
        (eg. pour filename=test1 on aura test1.txt et test1result.txt) 
    n = taille de la grille (eg. pour sudoku classique n=3)
    m = le nombre de case remplies qu'on veut (soit n**4 - m cases vides). """
    export_grille_txt(filename,n)
    prepare_sat(filename)
    cmd = ['minisat', SUDOKUDIR+filename+"dimac.cnf", SUDOKUDIR+filename+"result.txt"]
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    output, error = process.communicate()
    sudoku_mat = read_solution(filename,False)
    sudoku_liste = suppression(mat_to_list(sudoku_mat,n),n,m)
    result = format_to_string(sudoku_liste,n)
    export_result_txt(filename,result)    

if __name__ == "__main__":
	for m in range(0):
		gen_grille_finale("sudokugs",3,m)
