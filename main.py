#!/usr/bin/env python3
import sys, pickle, subprocess, math, random, string, time
import numpy as np
from os import listdir
from os.path import isfile, join
import matplotlib.pyplot as plt
from collections import namedtuple

#Directories
SUDOKUDIR = 'sudokus/'
STATSDIR = 'statistics/'

#encoding
code = string.digits+string.ascii_letters

#global variables to use before and after minisat
sudoku = []
n = 0
perm = []

#returns number char format
def char(n):
    return code[n]

def nfchar(c):
    return code.index(c)

#hashes sudoku
def sudokuhash(sudoku):
    return hash(tuple([tuple(x) for x in sudoku]))

#from ijk format to n variable for dimac file
def ijk_to_n(i,j,k,ns):
    return i+j*(ns**2)+k*(ns**2)**2+2

#from the variable number to ijk format to fitsudoku
def n_to_ijk(n,ns):
    n-=2
    return (n%(ns**2),(n//(ns**2))%(ns**2),n//((ns**2)**2))

#takes i (place in lvl) and lvl (heigh within calculator) and returns the number of the var
def i_n(i,lvl,nvar):
    if i >= nvar:
        return 1
    return i+(lvl+1)*nvar+2

#returns clauses needed to set var a equal to var b(xor)a in cnf dimac format
def xor(a, b, c): # a xor b = c
    if c == 1:
        return ""
    if b == 1:
        return eql(a, c)
    if a == 1:
        return eql(b ,c)
    cla = ""
    for temp in ["{} {} -{}","-{} -{} -{}","{} -{} {}","-{} {} {}"]:
        cla+=temp.format(a,b,c)
        cla+=" 0\n"
    return cla

#returns clauses needed to set var c equal to var b&a in cnf dimac format
def band(a, b, c): # a and b = c
    if c == 1:
        return ""
    if b == 1 or a == 1:
        return "-{} 0\n".format(c)
    return "-{} -{} {} 0\n {} -{} 0\n {} -{} 0\n".format(a,b,c,a,c,b,c)

#returns clauses needed to set var a equal to var b in cnf dimac format
def eql(a, b): # a = b
    if b == 1:
        return ""
    return "-{} {} 0\n {} -{} 0\n".format(a,b,a,b)

#returns number of lvls needed to count nvar variables
def glvls(nvar):
    return math.ceil(math.log2(nvar))

#returns number of variables needed to count nvar variables
def nvars(nvar):
    return nvar*(glvls(nvar)*3+2)+1

#retun numer of clauses added by calculator for nvar variables
def nclaus(nvar):
    lvls = glvls(nvar)
    return (nvar//2)*11*lvls+nvar

#creates dimac file in cnf form to later pass to minisat
def create_dimac(filename, n, sudoku = None, maxvar = None, perm = None):
    #modifies ijk_to_n function depending on perm
    if len(perm) > 0:
        ijk_to_n2 = lambda i,j,k,n: perm[0][ijk_to_n(i,j,k,n)-2]+2
    else:
        ijk_to_n2 = ijk_to_n(i,j,k,n)
    #failing miserably at counting number of clauses and variables
    n2 = n**2
    nvar = n2**3
    nclau = int((n2**2)*(3+n2*(n2-1))/2)
    with open(SUDOKUDIR+filename+"dimac.cnf","w") as file:
        file.write("c Here is a comment.\np cnf {} {}\n".format(nvars(nvar),(nclau+nclaus(nvar))*2))
        
        #to make sure there is at least one of each number per row column and block
        for k in range(n2):
            #rows and columns
            for i in range(n2):
                file.write(" ".join([str(ijk_to_n2(i,j,k,n)) for j in range(n2)])+" 0\n")
            for j in range(n2):
                file.write(" ".join([str(ijk_to_n2(i,j,k,n)) for i in range(n2)])+" 0\n")
            
            #blocks
            for ii in range(n):
                for jj in range(n):
                    file.write(" ".join([str(ijk_to_n2(ii*n+p%n,jj*n+p//n,k,n)) for p in range(n2)])+" 0\n")
        

        #no more than 2 per column row block
        for k in range(n2):
            #rows and columns
            for i in range(n2):
                for j in range(n2):
                    for jp in range(j+1,n2):
                        file.write("-"+" -".join([str(ijk_to_n2(i,j,k,n)), str(ijk_to_n2(i,jp,k,n))])+" 0\n")
            for j in range(n2):
                for i in range(n2):
                    for ip in range(i+1,n2):
                        file.write("-"+" -".join([str(ijk_to_n2(i,j,k,n)), str(ijk_to_n2(ip,j,k,n))])+" 0\n")
            
            #blocks
            for ii in range(n):
                for jj in range(n):
                    for mp in range(n2):
                        for np in range(mp+1,n2):
                            file.write("-"+" -".join([str(ijk_to_n2(ii*n+mp%n,jj*n+mp//n,k,n)), str(ijk_to_n2(ii*n+np%n,jj*n+np//n,k,n))])+" 0\n")



            

        #only one number per square
        for i in range(n2):
            for j in range(n2):
                for k in range(n2):
                    for l in range(k+1,n2):
                        file.write("-"+" -".join([str(ijk_to_n2(i,j,k,n)), str(ijk_to_n2(i,j,l,n))])+" 0\n")
        
        #initial conditions
        if sudoku != None: 
            for i in range(n2):
                for j in range(n2):
                    if sudoku[i][j] != -1:
                        file.write(str(ijk_to_n2(i,j,sudoku[i][j]-1,n))+" 0\n")
        #off cable 
        file.write("-1 0\n")
        
        for i in range(nvar):
            file.write("{} -{} 0\n".format(i+2,i+2+nvar))
        if maxvar == None:
            for i in range(nvar):
                file.write("-{} {} 0\n".format(i+2,i+2+nvar))

        #count number of variables
        if maxvar != None:
            lvls = glvls(nvar)
            for i in range(lvls):
                hop = 2**i
                for j in range(0,nvar,2*hop):
                    for k in range(hop):
                        file.write(band(i_n(j+k,3*i,nvar),i_n(j+k+hop,3*i,nvar),i_n(j+k,3*i+1,nvar)))
                        file.write(xor(i_n(j+k,3*i,nvar),i_n(j+k+hop,3*i,nvar),i_n(j+k,3*i+2,nvar)))
                    for k in range(2*hop):
                        if k == 0:
                            file.write(eql(i_n(j+k,3*i+2,nvar),i_n(j+k,3*i+3,nvar)))
                        elif k == hop:
                            file.write(eql(i_n(j+k-1,3*i+1,nvar),i_n(j+k,3*i+3,nvar)))
                        elif k < hop:
                            file.write(xor(i_n(j+k,3*i+2,nvar),i_n(j+k-1,3*i+1,nvar),i_n(j+k,3*i+3,nvar)))
                        else:
                            file.write(eql(1,i_n(j+k,3*i+3,nvar)))
            
            #maxvar condition
            x = bin(min(maxvar,nvar))[2:]
            x = "0"*(nvar - len(x)) + x
            v = nvars(nvar)
            for i in x:
                file.write("{}{} 0\n".format("-" if i =="0" else "", str(v)))
                v-=1

#reads a sudoku txt file and returns the matix of the sudoku
def parse(filename):
    sudoku = []
    with open(SUDOKUDIR+filename+".txt") as file:
        a = file.read()
        n = int(a[0])
        n2 = n**2
        a = a[2:]
        for i in range(n2):
            sudoku.append([])
            for j in range(n2):
                ind = i*(n2+1)+j
                if a[ind]=="*":
                    sudoku[i].append(-1)
                else:
                    sudoku[i].append(nfchar(a[ind]))
    return (sudoku, n)


#prepares the permutation and calls the dimac creating funciton
def prepare_sat(filename, genn = None , maxvar = None, useperm = True):
    global sudoku, n, perm
    if genn == None:
        sudoku, n = parse(filename)
    else:
        n = genn
        sudoku = [[-1]*n**2 for _ in range(n**2)]

    if useperm:
        tperm = np.random.permutation(n**6)
        perm = [{},{}]
        for i,v in enumerate(tperm):
            perm[0][i] = v
            perm[1][v] = i
    else:
        perm = None
    create_dimac(filename, n, sudoku, maxvar = maxvar, perm = perm)

#prints sudoku in the most beautiful way possible
def print_sudoku(sudoku,n = 3):
    print()
    for j,line in enumerate(sudoku):
        if j in [i*n for i in range(1,n)]:
            print("|".join([("".join(["\033[0;{};40m-".format(random.randint(31,37)) 
                            for _ in range(2*n+1)])) for _ in range(n)]))
        print("".join([(" \033[0;{};40m".format(31+x%7)+char(x) if x != -1 else "  " )
                    +("\033[0;{};40m |".format(random.randint(31,37)) 
                        if i in [i*n-1 for i in range(1,n)] else "")for i,x in enumerate(line)]))
    print()

#simply shows file
def show_file(filename):
    sudoku, n = parse(filename)
    print_sudoku(sudoku, n)

#splits string by strings in th seperators
def split(strin, separators = [" ","\n"]):
    sp = 0
    i = 0
    res = []
    while i < len(strin):
        if strin[i] in separators:
            res.append(strin[max(0,sp-1):i])
            sp = i
        i+=1
    return res

#splits string by strings in th seperators
def split_result(result, separators = [" ","\n"]):
    sp = 0
    i = 0
    res = []
    while i < len(result):
        if result[i] in separators:
            try:
                res.append(int(result[sp:i]))
            except:
                res.append(result[sp:i])
            sp = i
        i+=1
    return res

#saves sudoku of size n to filename.txt
def save_sudoku(filename, sudoku, n):
    txt = "{}\n".format(n)
    txt += "\n".join(["".join(["*" if var == -1 else char(var) for var in line]) for line in sudoku])
    with open(SUDOKUDIR+filename+".txt","w") as file:
        file.write(txt)

#takes the result string outputed from minisat and return state (satisfiability) and result (model if satisfiyable)
def parse_result(result):
    result = split_result(result)
    try:
        state = result[0]
    except:
        print(result)
        exit(1)

    result = result[1:]
    return (state,result)

def get_useful_vars(result,nvar,counted=False):
    try:
        return [i for i in result if 1 < i - (0 if not counted else nvar) < nvar+2]
    except e:
        print(e)
        print(result)


#reads the genresult.txt and saves it in .gen_n_maxvar_sudokuhash.txt
def save_gen(filename = "gen", maxvar = None):
    global n, perm
    nvar = n**6
    with open(SUDOKUDIR+filename+"result.txt") as file:
        state,result = parse_result(file.read())
    if state == "SAT":
        sudoku = [[-1]*n**2 for i in range(n**2)]
        for va in get_useful_vars(result,nvar,maxvar != None):
            if maxvar != None: va -= nvar
            va = perm[1][va-2]+2
            i, j, k = n_to_ijk(va,n)
            sudoku[i][j] = k+1
        
        numb = 0
        lvls = glvls(nvar)
        for i in result[-nvar-1:]:
            if nvars(nvar)-nvar < i:
                numb += (1<<(i-(nvars(nvar)-nvar)-1))
        
        if "gen" not in filename:
            save_sudoku(filename, sudoku, n)
        else:
            h = sudokuhash(sudoku)
            filename = "zzz"+filename+"_{}_{}_{}".format(n,numb,h)
            save_sudoku(filename, sudoku, n)
    return filename
    
#outputs the solution to the terminal for user
def output_solution(filename, maxvar = None):
    global sudoku, n, perm
    nvar = n**6
    print("\033[1;37;40mNow trying to solve "+filename+".txt :")
    print_sudoku(sudoku,n)
    with open(SUDOKUDIR+filename+"result.txt") as file:
        state,result = parse_result(file.read())
    if state == "UNSAT":
        print("Could not find solution")
        return
    print("\033[1;37;40mSolution found :")
    sudoku = [[-1]*n**2 for i in range(n**2)]
    for va in get_useful_vars(result,nvar,maxvar != None):
        if maxvar != None : va -= nvar
        va = perm[1][va-2]+2
        i, j, k = n_to_ijk(va,n)
        sudoku[i][j] = k+1

    print_sudoku(sudoku,n)
    
#returns None if no solution was found or sudoku if model exists
def read_solution(filename):
    """ Solve une grille de sudoku (a priori supposée satisfiable sinon retourne rien).
    Si possible la retourne sous forme d'une matrice, sinon ne retourne rien (= pb potentiel) """
    global sudoku, n, perm
    nvar = n**6
    with open(SUDOKUDIR+filename+"result.txt") as file:
        state,result = parse_result(file.read())
    if state == "UNSAT":
        return
    sudoku = [[-1]*n**2 for i in range(n**2)]
    for va in get_useful_vars(result,nvar):
        va = perm[1][va-2]+2
        i, j, k = n_to_ijk(va,n)
        sudoku[i][j] = k+1
    return sudoku

#running the dimac trough minisat
def run_minisat(filename):
    cmd = ["minisat","-rnd-init",SUDOKUDIR+filename+"dimac.cnf", SUDOKUDIR+filename+"result.txt"]
    process = subprocess.Popen(cmd, stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)#stdout=subprocess.PIPE)
    output, error = process.communicate()

#outputs the solution to the terminal for user
def print_result(result):
    global sudoku, n, perm
    nvar = n**6
    sudoku = [[-1]*n**2 for i in range(n**2)]
    for va in result[1:nvar+2]:
        if va > nvar+1 or va < 2:
            continue
        va = perm[1][va-2]+2
        i, j, k = n_to_ijk(va,n)
        sudoku[i][j] = k+1

    print_sudoku(sudoku,n)

#count number of solutions
def count_models(filename):
    counter = 0
    state = "SAT"
    while state == "SAT":
        run_minisat(filename)
        with open(SUDOKUDIR+filename+"result.txt") as file:
            state,result = parse_result(file.read())
        with open(SUDOKUDIR+filename+"dimac.cnf","a") as file:
            file.write(" ".join(str(-i) for i in result)+"\n")
        if state == "SAT":
            counter += 1    
    return counter

#runs count on per_maxvar sudokus starting from start and stops after maxtime seconds
def get_stats(outfile, idd = "", per_maxvar = 10, start = 63, maxtime = 5):
    outfile = idd + outfile
    try:
        with open(STATSDIR+outfile+'.pck','rb') as file:
            satgenstats, raiwimstats, satgennumbers, raiwimnumbers = pickle.load(file)
    except:
        satgenstats = [{} for _ in range(81)]
        raiwimstats = [{} for _ in range(81)]
        satgennumbers = [0 for _ in range(81)]
        raiwimnumbers = [0 for _ in range(81)]
        numbers = [{} for _ in range(81)]
    tstart = time.time()
    last_save = time.time()
    tfilename = "gen"
    for maxvar in range(start,0,-1):
        while satgennumbers[maxvar-1] < per_maxvar:
            if time.time() - tstart > maxtime:
                with open(STATSDIR+outfile+'.pck','wb') as file:
                    pickle.dump((satgenstats, raiwimstats, satgennumbers, raiwimnumbers),file)
                return
            elif time.time() - last_save > 60:
                with open(STATSDIR+outfile+'.pck','wb') as file:
                    pickle.dump((satgenstats, raiwimstats, satgennumbers, raiwimnumbers),file)
                last_save = time.time()
            prepare_sat(tfilename, genn = 3, maxvar = maxvar)
            run_minisat(tfilename)
            filename = save_gen(tfilename, maxvar)
            prepare_sat(filename)
            count = count_models(filename)
            satgenstats[maxvar-1][filename] = count
            satgennumbers[maxvar-1] += 1
        while raiwimnumbers[maxvar-1] < per_maxvar:
            if time.time() - tstart > maxtime:
                with open(STATSDIR+outfile+'.pck','wb') as file:
                    pickle.dump((satgenstats, raiwimstats, satgennumbers, raiwimnumbers),file)
                return
            elif time.time() - last_save > 60:
                with open(STATSDIR+outfile+'.pck','wb') as file:
                    pickle.dump((satgenstats, raiwimstats, satgennumbers, raiwimnumbers),file)
                last_save = time.time()
            filename = gen_grille_finale(tfilename, 3, maxvar)
            prepare_sat(filename)
            count = count_models(filename)
            raiwimstats[maxvar-1][filename] = count
            raiwimnumbers[maxvar-1] += 1
        

""" =========== GENERATION DE GRILLE SIMPLE A ETRE SOLVED =========== """

def position_aleatoire(n):
    """ Pour une grille de taille n génère une liste de taille n**2 de nombre aléatoires entre 0 et n**4.
    Ces nombres correspondent à l'emplacement d'un nombre k (entre 1 et n**2) sur la string qui génère la grille
    (cf. gen_grille). """
    return [random.randint(0,n**4-1) for k in range(n**2)]

def gen_grille(n):
    """ Étant donnée une liste d'emplacements des nombres k, crée une chaîne de caractères où:
    * = case vide
    k = nombre dans cette case
    \n = retour à la ligne pour former la grille. """
    cond_init_alea = position_aleatoire(n)
    s = [['*']*n**2 for _ in range(n**2)]
    for i,v in enumerate(cond_init_alea):
        s[v//n**2][v%n**2] = str(i+1)
    s = '\n'.join([''.join(line) for line in s])
    s = str(n) + '\n' + s
    return s


def export_grille_txt(filename,n):
    """ Exporte la grille en fichier texte lisible par le solveur de sudoku. La taille n de la grille
    est en début de fichier, avant la grille elle-même. """
    s_grille = gen_grille(n)
    file = open(SUDOKUDIR+filename + ".txt" , "w") 
    file.write(s_grille)
    file.close()

""" ========== SOLUTION GRILLE + SUPPRESSION ALEA DE CASES ============= """  

def suppression(sudoku,n,m):
    """ Tire au sort des positions (sous forme d'indices d'une liste). Remplace les nombres attribués à cette
    position par le SATsolver par une étoile. Retourne une nouvelle liste."""
    k = 0
    while k < n**4-m:
        i,j = (random.randint(0,n**2-1),random.randint(0,n**2-1))
        if sudoku[i][j] == -1:
            continue
        sudoku[i][j] = -1
        k += 1
    return sudoku

    memo = []
    while len(memo)<(n**4-m):
        i = random.randint(0,n**4-1)
        if i not in memo:
            memo.append(i)
    for j in memo:
        liste_solved[j] = '*'
    return liste_solved

def gen_grille_finale(filename,n,m):
    """ Créer un fichier texte contenant une grille incomplète, satisfiable.
    Variables:
    filename = nom qui auront les fichiers qu'on va créer 
    (eg. pour filename=test1 on aura test1.txt et test1result.txt) 
    n = taille de la grille (eg. pour sudoku classique n=3)
    m = le nombre de case remplies qu'on veut (soit n**4 - m cases vides). """
    export_grille_txt(filename,n)
    prepare_sat(filename)
    run_minisat(filename)
    sudoku = read_solution(filename)
    sudoku = suppression(sudoku,n,m)
    if "gen" in filename:
        h = sudokuhash(sudoku)
        filename = "yyygen_{}_{}_{}".format(n,m,h)
    save_sudoku(filename, sudoku, n)
    return filename

def maxvar_from_name(name):
    return int(split(name,["_"])[2])

def show_stats():
    files = [f for f in listdir(STATSDIR) if isfile(STATSDIR+f)]

    satgenstats, raiwimstats, satgennumbers, raiwimnumbers = [[],[],[],[]]

    for f in files:
        with open(STATSDIR + f, 'rb') as file:
            a,b,c,d = pickle.load(file)
            satgenstats.append(a)
            raiwimstats.append(b)
            satgennumbers.append(c)
            raiwimnumbers.append(d)
    satgenmin = [1000 for i in range(len(satgennumbers[0]))]
    satgenmax = [0 for i in range(len(satgennumbers[0]))]
    satgentotal = [0 for i in range(len(satgennumbers[0]))]
    satgenn = [0 for i in range(len(satgennumbers[0]))]
    raiwimmin = [1000 for i in range(len(satgennumbers[0]))]
    raiwimmax = [0 for i in range(len(satgennumbers[0]))]
    raiwimtotal = [0 for i in range(len(satgennumbers[0]))]
    raiwimn = [0 for i in range(len(satgennumbers[0]))]
    for i in range(len(satgenstats)):
        for maxvar,dic in enumerate(satgenstats[i]):
            for key,value in dic.items():
                if satgenmin[maxvar] > value:
                    satgenmin[maxvar] = value
                if satgenmax[maxvar] < value:
                    satgenmax[maxvar] = value
                satgentotal[maxvar] += value
                satgenn[maxvar] += 1
    for i in range(len(raiwimstats)):
        for maxvar,dic in enumerate(raiwimstats[i]):
            for key,value in dic.items():
                if raiwimmin[maxvar] > value:
                    raiwimmin[maxvar] = value
                if raiwimmax[maxvar] < value:
                    raiwimmax[maxvar] = value
                raiwimtotal[maxvar] += value
                raiwimn[maxvar] += 1

    satgenavg = [satgentotal[i]/satgenn[i] if satgenn[i] != 0 else 0 for i in range(len(satgentotal))]
    raiwimavg = [raiwimtotal[i]/raiwimn[i] if raiwimn[i] != 0 else 0 for i in range(len(raiwimtotal))]

    minn = min([i for i in range(len(satgenavg)) if satgenmin[i] != 1000 and raiwimmin[i] != 1000])
    maxx = max([i for i in range(len(satgenavg)) if satgenmin[i] != 1000 and raiwimmin[i] != 1000])

    x = [i+1 for i in range(minn,maxx+1)]

    ylabel = 'Nombre de solutions'
    xlabel = "Nombre d'entrées grille initiale"

    plt.plot(x,satgenavg[minn:maxx+1],label = "satgen")
    plt.plot(x,raiwimavg[minn:maxx+1],label = "raiwim")
    plt.ylabel(ylabel)
    plt.xlabel(xlabel)
    plt.title("Moyenne")    
    plt.legend()
    plt.show()
    plt.plot(x,satgenmin[minn:maxx+1],label = "satgen")
    plt.plot(x,raiwimmin[minn:maxx+1],label = "raiwim")
    plt.ylabel(ylabel)
    plt.xlabel(xlabel)
    plt.title("Min")    
    plt.legend()
    plt.show()
    plt.plot(x,satgenmax[minn:maxx+1],label = "satgen")
    plt.plot(x,raiwimmax[minn:maxx+1],label = "raiwim")
    plt.ylabel(ylabel)
    plt.xlabel(xlabel)
    plt.title("Max")    
    plt.legend()
    plt.show()

def statidinfo(idd):
    with open(STATSDIR + idd + "out.pck", 'rb') as file:
        a,b,c,d = pickle.load(file)
    print("Satgen : ",a,"\nRaiwim : ",b)
    print("Satgen : ",c,"\nRaiwim : ",d)


def retire_unique(filename, grille, n):
    "fonction recursive qui prend en valeur une grille de sudoku sous forme de matrice ayant une solution unique"
    "elle renvoie une matrice représentant un sudoku ayant une solution unique minimal"
    valeur_aleatoire = random.randint(0,80)
    (i,j) = (valeur_aleatoire//9,valeur_aleatoire%9)
    valeur_enleve = grille[i][j]
    grille[i][j] = '*'
    #export_result_txt("uniq",format_to_string(mat_to_list(grille,n),n))
    save_sudoku(filename, grille, n)
    prepare_sat(filename)
    if count_models(SUDOKUDIR+"uniq.txt") ==1 : 
        retire_unique(grille,n)
    else:
        grille[i][j] = valeur_enleve
        return(grille)
        
    
""" ========= FONCTION FINALE ========== """

def gen_grille_unique(filename,n):
    """ Créer un fichier texte contenant une grille incomplète, satisfiable.
    Variables:
    filename = nom qui auront les fichiers qu'on va créer 
    (eg. pour filename=test1 on aura test1.txt et test1result.txt) 
    n = taille de la grille (eg. pour sudoku classique n=3)
    m = le nombre de case remplies qu'on veut (soit n**4 - m cases vides). """
    export_grille_txt(filename,n)
    prepare_sat(filename)
    run_minisat(filename)
    sudoku = read_solution(filename)
    sudoku = retire_unique(filename, sudoku, n)
    if "gen" in filename:
        h = sudokuhash(sudoku)
        filename = "xxxgen_{}_{}_{}".format(n,m,h)
    save_sudoku(filename, sudoku, n)
    return filename

Command = namedtuple("Commmand", "cmd fields options")

#or line in 

if __name__ == "__main__":
    cmds = ["python3 main.py cmd",
                    "available commands: ",
                    "    solve filname",
                    "    satgen size maxvar number [outfilename]",
                    "    raiwim size maxvar number [outfilename]",
                    "    cmodels filename",
                    "    stats per_maxvar maxtime [id]",
                    "    showstats",
                    "    statidinfo id",
                    "    sebunik",
                    "    timeit"]

    #parsing input command
    if len(sys.argv) < 2:
        print("\n".join(cmds))
        exit(1)
    if sys.argv[1] == "timeit":
        maxvar = [i for i in range(64)]
        raiwimtimes = []
        satgentimes = []
        solveraiwimtimes = []
        solvesatgentimes = []
        filename = "timeit"
        for m in maxvar:
            raiwimtimes.append(0)
            satgentimes.append(0)
            solveraiwimtimes.append(0)
            solvesatgentimes.append(0)
            for i in range(3):
                print("i : ", i , " maxvar : ", m)
                start = time.time()
                prepare_sat(filename, 3, m)
                run_minisat(filename)
                save_gen(filename, maxvar)
                satgentimes[-1] += time.time() - start
                start = time.time()
                prepare_sat(filename)
                run_minisat(filename)
                save_gen(filename)
                solvesatgentimes[-1] += time.time()-start
                start = time.time()
                gen_grille_finale(filename, 3, m)
                raiwimtimes[-1] += time.time() - start
                start = time.time()
                prepare_sat(filename)
                run_minisat(filename)
                save_gen(filename)
                solveraiwimtimes[-1] += time.time()-start
        raiwimtimes[-1] /= 3
        satgentimes[-1] /= 3
        solveraiwimtimes[-1] /= 3
        solvesatgentimes[-1] /= 3
        
        plt.plot(maxvar, raiwimtimes, label = "raiwim generation times")
        plt.plot(maxvar, satgentimes, label = "satgen generation times")
        plt.legend()
        plt.show()
        plt.plot(maxvar, solveraiwimtimes, label = "raiwim solve times")
        plt.plot(maxvar, solvesatgentimes, label = "satgen solve times")
        plt.legend()
        plt.show()

                
        exit(1)
    elif sys.argv[1] == "sebunik":
        n = 3
        gen_grille_unique('sudoku_unique',3)
        exit(0)
    elif sys.argv[1] == "statidinfo":
        statidinfo(sys.argv[2])
        exit(0)
    elif sys.argv[1] == "showstats":
        show_stats()
        exit(0)
    elif sys.argv[1] == "raiwim":
        start = time.time()
        loop = int(sys.argv[4])
        filename = "gen" if len(sys.argv) < 6 else sys.argv[5]
        for _ in range(loop):
            gen_grille_finale(filename,int(sys.argv[2]),int(sys.argv[3]))
        print(time.time()-start," s")
    elif sys.argv[1] == "stats":
        filename = "gen"
        idd = sys.argv[4] if len(sys.argv) > 4 else ""
        get_stats("out", idd,int(sys.argv[2]),63,int(sys.argv[3]))
    elif sys.argv[1] == "cmodels":
        filename = sys.argv[2]
        prepare_sat(filename)
        count_models(filename)
    elif sys.argv[1] == "solve":
        start = time.time()
        filename = sys.argv[2]
        prepare_sat(filename)
        run_minisat(filename)
        print(time.time()-start," s")
        output_solution(filename)
    
    elif sys.argv[1] == "satgen":
        start = time.time()
        filename = "gen"
        loops = int(sys.argv[4])
        if len(sys.argv) > 5:
            filename = sys.argv[5]
        for i in range(loops):
            maxvar = int(sys.argv[3])
            genn = int(sys.argv[2])
            prepare_sat(filename, genn = genn, maxvar = maxvar)
            run_minisat(filename)
            save_gen(filename, maxvar)
        print(time.time()-start," s")
    else:
        print("\n".join(cmds))
        exit(1)

    
    #removing all temporary files
    cmd = ["rm",SUDOKUDIR+filename+"dimac.cnf", SUDOKUDIR+filename+"result.txt"]
    process = subprocess.Popen(cmd,stdout=subprocess.PIPE)
    output, error = process.communicate()
